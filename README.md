Beverding
=========

Simple macOS development setup.


Installation
------------

1. Install [Homebrew][].
2. Install [Ansible][] with the command `brew install ansible`.
3. Run this Ansible playbook with this command:

        ansible-playbook -i "localhost," -c local playbook.yml


[Ansible]: https://www.ansible.com/
[Homebrew]: https://brew.sh/

