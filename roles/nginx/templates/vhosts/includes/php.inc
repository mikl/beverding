location @rewrite {
  # Some modules enforce no slash (/) at the end of the URL
  # Else this rewrite block wouldn't be needed (GlobalRedirect)
  rewrite ^/(.*)$ /index.php?q=$1;
}

location ~ \.php$ {
  # Extra long timeout for running simpletests.
  fastcgi_read_timeout 600;

  fastcgi_split_path_info ^(.+\.php)(/.+)$;
  #NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
  include fastcgi_params;
  fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  fastcgi_intercept_errors on;
  fastcgi_pass 127.0.0.1:9000;
}
